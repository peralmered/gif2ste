## gif2ste

A script to convert a .gif to formats suitable for the Atari ST/STE.


#### v0.9

Now supports animated gifs. When fed an animated gif, output will contain all frames in sequence.


### Requirements

* Python v2.7.x

* Pillow (PIL fork)

      pip install pillow

* xia.py

    https://bitbucket.org/peralmered/xia_lib/src/master/


### Usage

    python gif2ste.py -i *input_file* -ao -b *number_of_bitplanes*


### All switches


#### -i [or --infile] *input_file* **required**

Input .gif file


#### -o [or --outfile] *output_file*

Expects a filename without extension; will add extensions automatically


#### -ao [or --autooutfile]

Will generate output filename from input file's name


#### -b [or --bitplanes] *number_of_bitplanes*

Determines the number of bitplanes in output. Single bitplane files
will be given **.1bp** file ending, 2-bitplane files will be given
**.2bp** file ending, 5-bitplane files will be given
**.5bp** file ending etc.


#### -p [or --padding] *padding_number*

Number of words (16-pixel blocks) to add to the right of each scanline


#### -np [or --nopal]

No palette will be saved. If this is not given, gif2ste will save
a **.pal** file with the palette


#### -ni [or --nointerleave]

Bitplanes are saved non-interleaved


#### -sp [or --strippath]

Path is stripped from output filename, so outfile is saved in current directory


#### -v [or --verbose]

Extra info during execution


