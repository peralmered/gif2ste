# -*- coding: utf-8 -*-

# Converts .GIF to STE format

strVersion="0.9"
strProgramHeader="---[ gif2ste v"+strVersion+" ]---------------------------------"
strProgramDescription="Converts .GIF to STE format"

#
# ToDo
# =====
#   # Argument for not padding lines to even 16 pixels width (cropping instead?)
#
#
# Done
# =====
#   # Parameter for outputting bitplanes non-interleaved
#   # Argument for padding line width
#
#

boolAllowScreenOutput=True

if boolAllowScreenOutput: print strProgramHeader

boolDebug=True

################################################################################################
## Defines

numBytesPerLine=16 # for .byte strings

NO_ERROR=0
numErrBit=1
FILE_EXISTS_FAIL=numErrBit
numErrBit*=2
FILE_READABLE_FAIL=numErrBit
numErrBit*=2

## Defines
################################################################################################
## Globals

numSteImageWords=None
numSteImageSize=None
numSteWordsPerLine=None
numSteBytesPerLine=None

## Globals
################################################################################################
## Constants

##
################################################################################################



################################################################################################
## Functions

def CheckFileExistsAndReadable(strFile):
  numOut=0
  boolHit=False
  if os.path.exists(strFile) and os.path.isfile(strFile):
    boolHit=True
  if not boolHit:
    numOut+=FILE_EXISTS_FAIL
  boolHit=False
  if os.access(strFile, os.R_OK):
    boolHit=True
  if not boolHit:
    numOut+=FILE_READABLE_FAIL
  return numOut


def GetBinaryFile(strFile):
  # Returns contents of binary file in array of ints, where each int represents a byte
  arrFile=[]
  byteThis=open(strFile, "rb").read()
  for b in byteThis:
    arrFile.append(ord(b))
  return arrFile


def WriteBinaryFile(arrData, strFile):
  # Writes binary data from arrData to binary file
  # Binary data is expected as an array of ints, where each int represents a byte
  fileOut=open(strFile, "wb")
  for i in range(len(arrData)):
    strThis=chr(arrData[i])
    fileOut.write(strThis)
  fileOut.close()


def WriteTextFile(arrData, strFile):
  # Writes data from arrData to textfile
  # Data is expected as an array of strings, where each string represents a line of text
  fileOut=open(strFile, "w")
  for strThis in arrData:
    fileOut.write(strThis+"\n")
  fileOut.close()


def Out(str):
  if boolAllowScreenOutput:
    sys.stdout.write(str)


def Fatal(str):
  # Fatal errors always output
  boolAllowScreenOutput=True
  Out("FATAL ERROR: "+str+"\n")
  exit(1)


def GetStringNybble(n):
  strNybble=str(bin(n))
  strNybble=strNybble.lstrip("0b")
  while(len(strNybble)<4):
    strNybble="0"+strNybble
  return strNybble


def ComponentToNybble(n):
  strN=GetStringNybble(n)
  strA=strN[3]
  strB=strN[:3]
  return int(eval("0b"+strA+strB))


def MakeSteCol(arrCol):
  r=arrCol[0]>>4
  g=arrCol[1]>>4
  b=arrCol[2]>>4
  finalCol=ComponentToNybble(r)*256
  finalCol+=ComponentToNybble(g)*16
  finalCol+=ComponentToNybble(b)
  return finalCol


def MakeDevpacWord(n):
  strN=hex(n)
  strN=strN.lstrip("0x")
  while(len(strN)<3):
    strN="0"+strN
  return "$"+strN


def MakeSTeData(arrImage):
  global numImageWidth
  global numImageHeight
  global numSteImageWords
  global numSteImageSize
  global numSteWordsPerLine
  global numSteBytesPerLine

  numOriginalImagePos=0
  arrFixedImage=[]
  for y in range(numImageHeight):
    for x in range(numImageWidth):
      thisPixel=arrImage[numOriginalImagePos]
      numOriginalImagePos+=1
      arrFixedImage.append(thisPixel)
    if not boolWidthIsDivisibleBy16:
      for x in range(numWidthAdjust):
        arrFixedImage.append(0)

  if not boolWidthIsDivisibleBy16:
    numFixedImageSize=len(arrFixedImage)

  # Convert to STE image
  numWordWidth=numNewWidth/16
  numImagePos=0
  arrSteImage=[]
  for y in range(numImageHeight):
    for word in range(numWordWidth):
      bitmask=32768
      arrBitplanes=[0,0,0,0]
      for n in range(16):
        thisPixel=arrFixedImage[numImagePos]
        numImagePos+=1
        strPixel=GetStringNybble(thisPixel)
        for i in range(4):
          if strPixel[3-i]=="1":
            arrBitplanes[i]+=bitmask
        bitmask/=2
      for i in range(numBitplanes):
        arrSteImage.append(arrBitplanes[i])
    if numPadding!=0:
      for i in range(numBitplanes):
        for n in range(numPadding):
          arrSteImage.append(0)


  # De-interleave data?
  if boolInterleavedOutput==False:
    # De-interleave bitplane data
    arrTemp=copy.deepcopy(arrSteImage)
    arrPlanes=[]
    for i in range(numBitplanes):
      arrPlanes.append([])
    numDataSize=len(arrTemp)/numBitplanes
    numPos=0
    numPlane0Size=0
    for i in range(numDataSize):
      for j in range(numBitplanes):
        arrPlanes[j].append(arrTemp[numPos])
        if j==0:
          numPlane0Size+=1
        numPos+=1
    arrSteImage=[]
    for thisPlane in arrPlanes:
      arrSteImage.extend(thisPlane)

  numSteImageWords=len(arrSteImage)
  numSteImageSize=numSteImageWords*2

  numSteWordsPerLine=numSteImageWords/numImageHeight
  numSteBytesPerLine=numSteImageWords/numImageHeight*2

  arrSteBytes=[]
  for i in range(len(arrSteImage)):
    thisWord=arrSteImage[i]
    thisHighByte=thisWord>>8
    thisLowByte=thisWord&0xff
    arrSteBytes.append(thisHighByte)
    arrSteBytes.append(thisLowByte)

  return arrSteBytes
  

##
################################################################################################



if boolDebug:
  print "Importing libraries...",
import sys
import os
from PIL import Image
from PIL import ImageColor
import argparse
import copy
if boolDebug:
  print "done\n"


# Test crap
if 1==2:
  for i in range(16):
    print hex(ComponentToNybble(i))
  exit()



objParser = argparse.ArgumentParser(description=strProgramDescription)

objParser.add_argument("-i", "--infile",
                       dest="infile",
                       help="file (with path) to load",
                       metavar="FILE",
                       required=True)
objParser.add_argument("-o", "--outfile",
                       dest="outfile",
                       help="file stub (with path) to save (can be replaced with --autooutfile or -ao to auto-generate output filename)",
                       metavar="FILE",
                       required=False)
objParser.add_argument("-ao", "--autooutfile",
                       dest="autooutfile",
                       help="automatically generate output filename",
                       action="store_const",
                       const=True,
                       required=False)
objParser.add_argument("-b", "--bitplanes",
                       dest="bitplanes",
                       help="number of bitplanes to save to (1-4, default is 4)",
                       metavar="NUMBER")
objParser.add_argument("-p", "--padding",
                       dest="padding",
                       help="word padding at the end of lines (default is 0)",
                       metavar="NUMBER")
objParser.add_argument("-np", "--nopal",
                       dest="nopal",
                       help="don't save palette file",
                       action="store_const",
                       const=True,
                       required=False)
objParser.add_argument("-ni", "--nointerleave",
                       dest="nointerleave",
                       help="save bitplanes non-interleaved",
                       action="store_const",
                       const=True,
                       required=False)
objParser.add_argument("-sp", "--strippath",
                       dest="strippath",
                       help="strip path from output, i e save in current dir",
                       action="store_const",
                       const=True,
                       required=False)
objParser.add_argument("-v", "--verbose",
                       dest="verbose",
                       help="shows additional information during processing",
                       action="store_const",
                       const=True,
                       required=False)
                       #action="store_true",
                       #default=False)

boolInFileGiven=False
boolOutFileGiven=False
boolVerboseMode=False
numBitplanes=4
numPadding=0
boolMakePalette=True
boolAutoFilename=False
boolStripOutputPath=False
boolInterleavedOutput=True

numArgs=0
Args=vars(objParser.parse_args())

for key in Args:
  numArgs+=1
  #print ">> "+str(key)+" = "+str(Args[key])
  strValue=str(Args[key])
  if key=="infile" and strValue!="None":
    boolInFileGiven=True
    strInFile=strValue
  elif key=="outfile" and strValue!="None":
    boolOutFileGiven=True
    strOutFile=strValue
  elif key=="autooutfile" and strValue!="None":
    boolAutoFilename=True
  elif key=="strippath" and strValue!="None":
    boolStripOutputPath=True
  elif key=="bitplanes" and strValue!="None":
    numBitplanes=int(strValue)
  elif key=="padding" and strValue!="None":
    numPadding=int(strValue)
  elif key=="nopal" and strValue!="None":
    boolMakePalette=False
  elif key=="nointerleave" and strValue!="None":
    boolInterleavedOutput=False
  elif key=="verbose" and strValue!="None":
    boolVerboseMode=True

# No args = print usage and exit
if numArgs==0:
  objParser.print_help()
  exit(0)

# Illegal combination of --outfile and --autooutfile
if boolOutFileGiven:
  if boolAutoFilename:
    Fatal("Both --outfile and --autooutfile options given - choose one, please!")
if not boolOutFileGiven:
  if not boolAutoFilename:
    Fatal("Neither --outfile or --autooutfile option given - choose one, please!")


# Test number of bitplanes
if numBitplanes<1:
  Fatal("Number of bitplanes must be in the range 1-4!")
if numBitplanes>4:
  Fatal("Number of bitplanes must be in the range 1-4!")

# Test padding
if numPadding<0:
  Fatal("Padding value must be positive!")

# Test infile
numResult=CheckFileExistsAndReadable(strInFile)
if numResult!=0:
  if numResult&FILE_EXISTS_FAIL:
    Fatal("Can't find file \""+strInFile+"\"!")
  if numResult&FILE_READABLE_FAIL:
    Fatal("Can't read file \""+strInFile+"\"!")

if boolOutFileGiven:
  strOutFileImage=strOutFile+"."+str(numBitplanes)+"bp"
  strOutFilePalette=strOutFile+".pal"
if boolAutoFilename:
  strOutFileImage=strInFile[:-4]
  strOutFilePalette=strOutFileImage+".pal"
  strOutFileImage=strOutFileImage+"."+str(numBitplanes)+"bp"

if boolStripOutputPath:
  strOutFilePalette=os.path.basename(strOutFilePalette)
  strOutFileImage=os.path.basename(strOutFileImage)

Out("Reading input file \""+strInFile+"\"...")
arrInFile=GetBinaryFile(strInFile)
imgIn=Image.open(strInFile)
Out("done.\n")

numColorTableByte=arrInFile[10]
if numColorTableByte&128==0:
  Fatal("\""+strInFile+"\" doesn't contain a global color table!")
numPaletteSize=numColorTableByte&0b111


if numPaletteSize==0:
  numPaletteSize=2
elif numPaletteSize==1:
  numPaletteSize=4
elif numPaletteSize==2:
  numPaletteSize=8
else:
  numPaletteSize=16
  if numPaletteSize>3:
    if boolMakePalette:
      Out("Notice: Palette too large; truncated to 16 colors\n")

numPalFileOffset=13
arrGifPal=[]
arrStePal=[]
for i in range(numPaletteSize):
  thisCol=arrInFile[numPalFileOffset:numPalFileOffset+3]
  arrGifPal.append(thisCol)
  numPalFileOffset+=3
  numSteCol=MakeSteCol(thisCol)
  arrStePal.append(MakeDevpacWord(numSteCol))
  #print str(thisCol)+" - "+hex(numSteCol)+" -- "+MakeDevpacWord(numSteCol)

for i in range(16-numPaletteSize):
  thisCol=[0xf0, 0x00, 0xf0]
  numSteCol=MakeSteCol(thisCol)
  arrStePal.append(MakeDevpacWord(numSteCol))

arrImage=list(imgIn.getdata()) # one int per pixel, value=palette index, so no conversion necessary!
numImageSize=len(arrImage)

arrImageDimensions=imgIn.size
numImageWidth=arrImageDimensions[0]
numImageHeight=arrImageDimensions[1]

if 1==2:
  arrPalette=imgIn.getcolors()
  
  #print arrPalette
  #print len(arrPalette)
  arrPal=[]
  for i in range(len(arrPalette)):
    thisEntry=arrPalette[i]
    numColor=thisEntry[0]
    numPalPos=thisEntry[1]
    #print str(i)+": "+"pos "+str(numPalPos)+" - "+str(numColor)
    arrPal.append(numPalPos)
  #tempPal=imgIn.palette
  
  numPaletteSize=len(arrPalette)
  assert imgIn.mode == "P"
  tempPalette = imgIn.resize((256, 1))
  tempPalette.putdata(range(256))
  tempPalette = tempPalette.convert("RGB").getdata() 
  # tempPalette now contains a sequence of (r, g, b) tuples
  arrPalette=list(tempPalette)[:numPaletteSize]
  
  #print arrPalette
  
  print "Palette:"
  for i in range(len(arrPalette)):
    print str(arrPal[i])+": "+str(arrPalette[i])+" - "+hex(MakeSteCol(arrPalette[i]))
  print


# Count number of frames
objFrame=Image.open(strInFile)
arrGifFrames=[]
numFrames=0
while objFrame:
  arrGifFrames.append(copy.deepcopy(objFrame))
  numFrames+=1
  try:
    objFrame.seek(numFrames)
  except EOFError:
    break;


if boolVerboseMode:
  Out("Image data:\n")
  Out("  Number of frames: "+str(numFrames)+"\n")
  Out("  Colors in palette: "+str(numPaletteSize)+"\n")
  Out("  Pixels in image: "+str(numImageSize)+"\n")
  Out("  Image size: "+str(numImageWidth)+" x "+str(numImageHeight)+"\n")


boolWidthIsDivisibleBy16=False
if numImageWidth%16.0==0:
  boolWidthIsDivisibleBy16=True
  numNewWidth=numImageWidth
else:
  numWidthAdjust=16-(numImageWidth%16)
  numNewWidth=numImageWidth+numWidthAdjust


#if numImageWidth%16.0==0:
if boolWidthIsDivisibleBy16==False:
  if boolVerboseMode:
    Out("    Note: Width is zero-padded to "+str(numNewWidth)+" to make it word-divisible!\n")


arrOutData=[]
for thisFrame in arrGifFrames:
  arrBytes=MakeSTeData(thisFrame.getdata())
  arrOutData.extend(arrBytes)


if boolMakePalette:
  strStePal=""
  strStePal+="  ; Palette from \""+strInFile+"\"\n"
  numPalCol=0
  for line in range(2):
    strStePal+="    dc.w "
    for word in range(8):
      strStePal+=arrStePal[numPalCol]
      numPalCol+=1
      if not word==7:
        strStePal+=", "
    strStePal+="\n"


if boolVerboseMode:
  Out("\n")
  Out("STE image data:\n")
  Out("  Number of frames: "+str(numFrames)+"\n")
  Out("  Bitplanes: "+str(numBitplanes)+"\n")
  Out("  Lines per frame: "+str(numImageHeight)+"\n")
  Out("  Lines total: "+str(numImageHeight*numFrames)+"\n")
  Out("  Words per line: "+str(numSteWordsPerLine)+" ("+str(numBitplanes)+" bitplanes)\n")
  Out("  Bytes per line: "+str(numSteBytesPerLine)+" ("+str(numBitplanes)+" bitplanes)\n")
  Out("  Output size: "+str(len(arrOutData))+" bytes\n")
  if boolMakePalette:
    Out("STE palette:\n")
    print strStePal

strPerFrame=""
if numFrames>1:
  strBytesPerFrame=str(len(arrOutData)/numFrames)
  strPerFrame=" ("+strBytesPerFrame+" bytes per frame)"

Out("Writing image file \""+strOutFileImage+"\"...")
WriteBinaryFile(arrOutData, strOutFileImage)
Out("done. "+str(len(arrOutData))+" bytes written"+strPerFrame+".\n")

if boolMakePalette:
  numPalSize=len(strStePal)
  arrStePal=strStePal.split("\n")
  Out("Writing palette file \""+strOutFilePalette+"\"...")
  WriteTextFile(arrStePal, strOutFilePalette)
  Out("done. "+str(numPalSize)+" bytes written.\n")


